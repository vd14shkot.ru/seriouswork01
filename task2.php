<?php

function createFileWithSum(string $pathToFiles): void {
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles
    if(!is_dir($pathToFiles)) {
        return;
    }

    $arrayone = file($pathToFiles . '/1.txt');
    $arraytwo = file($pathToFiles . '/2.txt');
    foreach ($arrayone as $index => $numberone) {
        $numbertwo = $arraytwo[$index];
        $arraythree[$index] = $numberone + $numbertwo;
    }
    file_put_contents($pathToFiles . '/3.txt', implode(PHP_EOL, $arraythree));
}